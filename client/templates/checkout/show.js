Template.checkoutShow.helpers({
  cart : function(){
    currentCart = Carts.findOne({userKey : userKey});
    return currentCart;
  }
});

Template.checkoutShow.onRendered(function(){

  //wire up KO and some test data
  checkoutModel = new CheckoutViewModel({
    name : "Jake Lee",
    email : "jake481lee@gmail.com",
    address : {
      street : "1234 Hire Me St.",
      city : "Seattle",
      state : "WA",
      zip : "98101",
      country : "US"
    }
  });

  var panel = document.getElementById("checkout-panel");
  ko.cleanNode(panel);
  ko.applyBindings(checkoutModel,panel);

});