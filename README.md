![seattlegrace - Copy2.png](https://bitbucket.org/repo/BKE6rg/images/3551916895-seattlegrace%20-%20Copy2.png)This is an ecommerce storefront I built for my wife to display and sell her preserved floral arrangements, all natural candles, and leather accessories.

It is built using Meteor.js, Knockout.js, MongoDB, Stripe, PostgreSQL.

A production version is in the works. Stay tuned.