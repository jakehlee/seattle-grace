var productSeeds = [
    {
        "sku" : "cswf",
        "name" : "Classical Style with White Frame",
        "image" : "f1.jpg",
        "summary" : "A 3D potted plant arrangement with refreshing spring hues.",
        "description": "This piece features minty hues and orange and yellow to accent its fresh look. Hang it on the wall or prop it on a shelf, it will surely freshen and lighten the look of any space.",
        "price" : 11999,
        "inventory" : 22,
        "status" : "published",
        "published_at" : new Date(),
        "cost" : 3345,
        "vendor" : {
          "id" : 2,
          "slug" : "framed",
          "name" : "Framed"
        }
    },
    {
        "sku" : "cswc",
        "name" : "Classy and Simple Wall Clock",
        "image" : "f2.jpg",
        "summary" : "An enclosed Wall Clock with a face that features both classic roman numerals and numbers.",
        "description": "This piece is elegant and simple with its 5 roses and pine branches hugging the clock face. Perfect for hanging or propping, this one is sure to add understated elegance to any space.",
        "price" : 14999,
        "inventory" : 22,
        "status" : "published",
        "published_at" : new Date(),
        "cost" : 3345,
        "vendor" : {
            "id" : 2,
            "slug" : "framed",
            "name" : "Framed"
        }
    },
    {
        "sku" : "mrb",
        "name" : "Monogrammed in Roses Box",
        "image" : "f3.jpg",
        "summary" : "A monogrammed arrangement of roses for that special someone.",
        "description": "This is the perfect gift for that special someone. More personal and unique than any other gift of flowers. Be sure to customize your own now.",
        "price" : 19999,
        "inventory" : 99,
        "status" : "published",
        "published_at" : new Date(),
        "cost" : 65332,
        "vendor" : {
            "id" : 2,
            "slug" : "framed",
            "name" : "Framed"
        }
    },
   /* {
        "sku" : "vldc",
        "name" : "Violet and Lavender Desk Clock",
        "image" : "3.jpg",
        "summary" : "A clock that is easy on the eyes",
        "description": "This clock will surely relax and soothe you with its lavender and violent pastel tones. Perfect as a gift for a loved one or to add charm to any space around the house.",
        "price" : 4999,
        "inventory" : 99,
        "status" : "published",
        "published_at" : new Date(),
        "cost" : 65332,
        "vendor" : {
            "id" : 2,
            "slug" : "framed",
            "name" : "Framed"
        }
    },*/
/*    {
        "sku" : "opprrc",
        "name" : "Orange and Pastel Pink Rose Rectangular Centerpiece",
        "image" : "1.jpg",
        "summary" : "A lovely, inspiring centerpiece perfect for any occasion",
        "description": "This piece features one large and two small orange roses along with two medium and one small pastel pink rose. The verdant moss is preserved as well with berries and pine cones rounding out the piece. A truly lovely addition to your dining table setting. ",
        "price" : 12999,
        "inventory" : 43,
        "status" : "published",
        "published_at" : new Date(),
        "cost" : 67743,
        "vendor" : {
            "id" : 1,
            "slug" : "centerpieces",
            "name" : "Centerpieces"
        }
    },*/
    {
        "sku" : "lvsc",
        "name" : "Lavender & Violet Square Centerpiece",
        "image" : "c1.jpg",
        "summary" : "A relaxing lavender centerpiece with flower pot",
        "description": "The colors and style of this centerpiece are sure to bring calm and relaxation to any space. Complete with a matching square porcelain flower pot, this one is not to be missed. ",
        "price" : 11999,
        "inventory" : 43,
        "status" : "published",
        "published_at" : new Date(),
        "cost" : 67743,
        "vendor" : {
            "id" : 1,
            "slug" : "centerpieces",
            "name" : "Centerpieces"
        }
    },
    {
        "sku" : "rrwbc",
        "name" : "Red Rose Woven Basket Centerpiece",
        "image" : "c2.jpg",
        "summary" : "A traditional arrangement of various roses in a woven basket.",
        "description": "This piece has a open meadow feel to it but remains traditional with the red rose as the focal point.Perfect on a dining table or shelf or outside on a porch or balcony. ",
        "price" : 8999,
        "inventory" : 43,
        "status" : "published",
        "published_at" : new Date(),
        "cost" : 67743,
        "vendor" : {
            "id" : 1,
            "slug" : "centerpieces",
            "name" : "Centerpieces"
        }
    },
    {
        "sku" : "rcc",
        "name" : "Round Candle Centerpiece",
        "image" : "c3.jpg",
        "summary" : "A pedestal centerpiece with a candle at its center",
        "description": "The sky blue tones add a heavenly touch to this unique centerpiece. Available with a matching single blue rose in a bowl.",
        "price" : 17999,
        "inventory" : 43,
        "status" : "published",
        "published_at" : new Date(),
        "cost" : 67743,
        "vendor" : {
            "id" : 1,
            "slug" : "centerpieces",
            "name" : "Centerpieces"
        }
    },

    {
        "sku" : "psbhw",
        "name" : "Purple and Sky Blue Heart Wreath",
        "image" : "w1.jpg",
        "summary" : "A cupid inspired wreath of moss",
        "description": "The incredible attention to detail with the texture of the moss and the arrow like branch as well as the wildberry colors all make this an absolute one of a kind must have wreath. ",
        "price" : 8999,
        "inventory" : 43,
        "status" : "published",
        "published_at" : new Date(),
        "cost" : 67743,
        "vendor" : {
            "id" : 3,
            "slug" : "wbs",
            "name" : "Wreaths & Bouquets"
        }
    },
    {
        "sku" : "fgw",
        "name" : "Fresh Greens Wreath",
        "image" : "w2.jpg",
        "summary" : "A fresh and clean wreath embodying spring",
        "description": "This wreath resembles a piece of a secret elven forest. Refreshing, clean, and full of life, it is sure to add freshness and new life to any space.",
        "price" : 8999,
        "inventory" : 43,
        "status" : "published",
        "published_at" : new Date(),
        "cost" : 67743,
        "vendor" : {
            "id" : 3,
            "slug" : "wbs",
            "name" : "Wreaths & Bouquets"
        }
    },
    {
        "sku" : "ssc",
        "name" : "Strawberry Smoothie Candle",
        "image" : "cnd1.jpg",
        "summary" : "A candle that looks absolutely delicious",
        "description": "This candle was made with all natural soy waxes and essential oils. It is sure to please with its tarty scent and delicious texture. ",
        "price" : 2999,
        "inventory" : 43,
        "status" : "published",
        "published_at" : new Date(),
        "cost" : 67743,
        "vendor" : {
            "id" : 4,
            "slug" : "candles",
            "name" : "Candles"
        }
    }

];

if(Products.find().count() === 0){
  _.each(productSeeds, function(product){
    Products.insert(product);
    console.log("Inserted ", product.sku);
  })
}

if(Meteor.users.find().count() === 0){
  var id = Accounts.createUser({
    username : "Administrator",
    email: "admin@test.com",
    password: "admin123",
    profile: { name: "Big Admin" },
    roles : []
  });

  Roles.addUsersToRoles(id, ["Administrator"]);
  console.log("Added Admin user...");
}

if(Vendors.find().count() === 0){
  var vendors = [
    {
      id : 1,
      slug: "centerpieces",
      name : "Centerpieces",
      description: "Our tasteful centerpieces will add charm and liven up any table or countertop."
    },
    {
      id : 2,
      slug: "framed",
      name : "Framed",
      description: "Our framed pieces can be hung on a wall or propped on a table or shelf to add texture and mood to any space."
    },
    {
      id : 3,
      slug: "wbs",
      name : "Wreaths & Bouquets",
      description : "Whether for a wedding or party, our wreaths and bouquets will help you cherish any occasion and never go out of season."
    },
    {
      id : 4,
      slug: "candles",
      name : "Candles",
      description : "Exclusively handmade from the finest quality all natural ingredients, you can rest assured our candles will leave you infused and enthused."
    }
  ];

  _.each(vendors, function(vendor){
    Vendors.insert(vendor);
    console.log("Added ", vendor.name);
  });
}